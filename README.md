# Spring Boot Ci

Demo of Spring Boot with CI using gitlab-ci

## Software requirements

- Gitlab account
- Git
- Java
- IDE
- Maven (possibly bundled with IDE)

## Knowledge requirements

- Git
- Java language
- General computer usage

## Getting started

- Fork the repository
    - Using the Gitlab website:
        - Press "Fork" (top-right)
        - Enter your own target workspace
        - Select visibility as you like
- Clone your own repository

### Creating a Spring Boot project

- Go to https://start.spring.io/
- Enter a nice sounding Group Name
- Enter a nice sounding Artifact 
- Optionally enter a name and description
- Select dependencies as you wish (this can be changed later)
    - Recommended:
        - Lombok (Developer tools for faster development)
        - Spring Web (Embedded Webserver)
        - Spring Boot Actuator (Monitor and management of running application)
        - Apache Freemarker (Templating engine)
    Feel free to select more if you want more features
- Press generate to download a zip of the generated code
- Unzip the code and move it in the Git repository
- Add and commit the new files as a working starting point

### Adding functionality

- Add Spring beans and POJOs as needed/wanted
